<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enlistee;

class EnlisteeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $req)
    {
        $enlistees = Enlistee::all();
        return response($enlistees,200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $req)
    {
        $req->validate([
            'CID'=>'required|max:11',
            'name'=>'required|max:255',
            'gender'=>'required:max:255',
            'gewogId'=>'required'
        ]);

        Enlistee::create([
            'CID'=>$req->CID,
            'name'=>$req->name,
            'gender'=>$req->gender,
            'gewogId'=>$req->gewogId
        ]);

        return response([
            'message'=>'Enlistee added!'
        ],200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $enlistee = Enlistee::findOrFail($id);
        return response($enlistee,200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $req, string $id)
    {
        $enslistee = Enlistee::findorFail($id);
        $enlistee->update([
            'CID'=>$req->CID,
            'name'=>$req->name,
            'gender'=>$req->gender,
            'gewogId'=>$req->gewogId
        ]);

        return response([
            'message'=>'Updated!'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $enlistee = Enlistee::where('CID',$id)->delete();
        return response([
            'message'=>'Deleted!'
        ],200);
    }
}
