<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Mail;

class AuthenticationController extends Controller
{
    public function register(Request $req) {
        $req->validate([
            'name'=>'required|max:255',
            'email'=>'required|unique:users|max:255',
            'password'=>'required|min:8'
        ]);

        $user = User::create([
            'name'=>$req->name,
            'email'=>$req->email,
            'password'=>Hash::make($req->password)
        ]);

        $token = $user->createToken('auth_token')->accessToken;

        return response([
            'token'=>$token
        ]);
    }

    public function login(Request $req) {
        $req->validate([
            'email'=>'required',
            'password'=>'required'
        ]);

        $user = User::where('email',$req->email)->first();

        if (!$user || !Hash::check($req->password,$user->password)) {
            return response([
                'message' => 'The Credentails are Incorrect!',
            ]);
        }

        $token = $user->createToken('auth_token')->accessToken;

        return response([
            'token'=>$token
        ]);
    }

    public function logout(Request $req) {
        $req->user()->token()->revoke();

        return response([
            'message'=>'Logged Out Successfully!'
        ]);
    }

    public function generateFourDigitCode() {
        $code = mt_rand(1000, 9999);
    
        return $code;
    }    

    public function sendMail(Request $req) {
        $email = $req->email;
        $code = $this->generateFourDigitCode();
        $content = "Use this code: ".$code;
    
        try {
            Mail::send('forgotpassword', ['messages' => $content], function($message) use ($email) {
                $message->to($email)->subject("Password Reset");
            });
    
            return response()->json([
                'code'=>$code,
                'message' => 'Email sent Successfully'
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Email sending Failed: ' . $e->getMessage()
            ], 500);
        }
    }
    
}
