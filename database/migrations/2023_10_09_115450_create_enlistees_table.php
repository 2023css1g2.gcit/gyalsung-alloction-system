<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('enlistees', function (Blueprint $table) {
            $table->id('CID');
            $table->string('name');
            $table->string('gender');
            $table->integer('gewogId');
            $table->timestamps();
            $table->foreign('gewogId')
                ->references('gewogId')
                ->on('gewog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('enlistees');
    }
};
